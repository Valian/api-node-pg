const pool = require("../config/db");

const getUsers = async (req, res) => {
  let result = await pool.query("select * from usuarios");

  return res.status(200).json(result.rows);
};

const getUsersById = async (req, res) => {
  id = req.params.id;
  let result = await pool.query("select * from usuarios where id = $1", [id]);

  return res.status(200).json(result.rows);
};

const nuevoUsuario = async (req, res) => {
    const { name, direccion, email } = req.body;


  try {
    await pool.query(
      "INSERT INTO usuarios (name, direccion, email) VALUES ($1, $2, $3)",
      [name, direccion, email]
    );
    return res.status(200).json("ok");
  } catch (e) {
    return res.status(404).json(e);
  }
};
const putUser = async (req, res) => {
  const { name, direccion, email } = req.body;
  const id = req.params.id;
  console.log(req.body);
  try {
    await pool.query(
      "UPDATE usuarios SET name = $1, direccion = $2, email = $3 WHERE id = $4",
      [name, direccion, email, id]
    );
    return res.status(200);
  } catch (e) {
    return res.status(404).json(e);
  }
};

const deleteUser = async (req, res) => {
  const id = req.params.id;
  try {
    await pool.query("delete * WHERE id = $4", [id]);
    return res.status(200);
  } catch (e) {
    return res.status(404).json(e);
  }
};

module.exports = { getUsers, getUsersById, nuevoUsuario, putUser, deleteUser };
