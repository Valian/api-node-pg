//importaciones
const express = require('express')
const morgan = require('morgan')

//inicializador
const app = express()
app.set("port", 3000)
require('./config/db')

//middleware
app.use(morgan('dev'))
app.use(express.json())

//rutas
app.use(require('./routes/index'))

app.listen(app.get('port'), () => {
    console.log("Servidor corriendo, puerto ", app.get("port"));
})