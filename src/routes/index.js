const express = require("express");
const router = express.Router();

const UsuariosController = require('../controllers/UsuariosController')

router.get("/", UsuariosController.getUsers);
router.get("/:id", UsuariosController.getUsersById);
router.post("/", UsuariosController.nuevoUsuario);
router.put("/:id", UsuariosController.putUser);
router.delete("/:id", UsuariosController.deleteUser);


module.exports = router;